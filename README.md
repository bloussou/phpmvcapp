# My application
In this repo you will find the project of Brieuc Loussouarn.

## How to run the app
To run the app you have to :
* clone this repo
* open a terminal in this directory
* execute `docker-compose build`
* then execute `docker-compose up`

## My choice
To develop this app I have made different choice. I have chosen to use docker, and after I have understood the principal principles of PhP to use the MVC pattern to structure my app.

### Pattern MVC
As you will see in the `./public_html` folder this app contains different folder.


#### View
The views are the only files with php and html.

I have set up 4 differents views :
* __template__ :

Template is just to set up the structure of the page with the necessary tags. 
It also sets the title and the content. This view is required in the other views.
* __detailsView__ and __listView__ :

They are structured in the same way. Firstly I set the title of the page.

Then I use `ob_start()` : This function will turn output buffering on. While output buffering is active no output is sent from the script (other than headers), instead the output is stored in an internal buffer.

Finally I set up the `$content` variable and require __template.php__.

* __formView__ :

This one only contains html.

#### Controller
In the controller folder I have set up the controller **frontend.php**. This controller is the link between the views and the models.
The doc in the file.

#### Model
I have set up to model for the app because I have chosen to use two ways to call the github API. To do the call I use the GuzzleHttp composer package.
They do the call to the API and catch the error if one occures. 
* __DetailsCommit__
* __ListCommits__

The documentation is in the files.

#### Index.php controller
This is the main controller of my app. According to the value of the `$_GET` and `$_POST` it sets the views that have to be displayed.

### Introduce a bit of OOP
I have just used class in my models but I have read that it is possible to do real OOP with PHP. I have not used interfaces or other things because this project is simple. But this could be a way to improve the app.

## Time spent in details : 7h of coding
* __Read Documentation__ : 2h

I have begun with no idea of how to develop an app with PHP. So I have read documentation to start during one hour.

Then at the end I have spent 1h to read the good practices in PhP with the [PSR1](https://www.php-fig.org/psr/psr-1/) and [PSR2](https://www.php-fig.org/psr/psr-2/).

* __Set up the docker environment__ : 1h
* __Develop the "dirty" app__ : 4h

I have spent four hours to develop a "dirty" app that function with all the current features.

* __Refactor the code__ : 2h

I have spent 2 hours to set up the MVC pattern and build a real functional and maintainable app.

* __Write Documentation__ : 2h

I have spent 2hours to write the readlme, bonus files and add some comments in the project.

## Possible Improvements

### Add features
My goal is to show you that I can write a maintable and well coded app in a short time (MVC, OOP...). So I haven't implemented all the features you propose. 

### Improve the design
The design of the list is not too bad, but I know that the design of the details is really hugly.

### Multiple Pages
I don't know if it is a good way of developping a PhP app to use index.php to display different views as I have done it.

### Use other libraries
I haven't got the time to integer vuejs, Semantic UI...
