<?php
require 'vendor/autoload.php';

class ListCommits
{
    /*
    getCommits return the result to the api github with this type of url :
    "https://api.github.com/repos/torvalds/linux/commits"
    */
    function getCommits($owner, $repos) 
    {
        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', 'https://api.github.com/repos/'.$owner.'/'.$repos.'/commits');
            $body = $res->getBody();
            return json_decode($body, true);
        } catch (Exception $e) {
            die('Erreur : '.$e->getMessage());
        }
    }
}
