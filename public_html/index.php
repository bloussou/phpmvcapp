<?php
require('controller/frontend.php');

if (isset($_GET['url'])) {
    detailsCommit($_GET['url']);
} 
else {
    displayForm();
    if (is_null($_POST['owner']) ||is_null($_POST['repos']) || $_POST['repos'] === "" || $_POST['owner'] === "") {
        listCommits();
    }
    else {
        listCommits($_POST['owner'], $_POST['repos']);
    }
}